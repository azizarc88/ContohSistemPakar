package id.backbonedev.contohsistempakar;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.style.BulletSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class HasilAnalisaActivity extends AppCompatActivity
{
    private ImageView IVGambar;
    private TextView LKeterangan, LPenyakit;
    private Button BUlangi;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_analis);

        IVGambar = (ImageView) findViewById(R.id.IVGambar);
        LKeterangan = (TextView) findViewById(R.id.LKeterangan);
        LPenyakit = (TextView) findViewById(R.id.LPenyakit);
        BUlangi = (Button) findViewById(R.id.BUlangi);

        IVGambar.setImageResource(getIntent().getIntExtra("gambar", 0));
        LKeterangan.setText(getIntent().getStringExtra("keterangan"));
        LPenyakit.setText(getIntent().getStringExtra("penyakit"));

        BUlangi.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });
    }
}
