package id.backbonedev.contohsistempakar.Database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import id.backbonedev.contohsistempakar.R;

/**
 * Created by Aziz Nur Ariffianto on 0025, 25 Jul 2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper
{
    private static final String DATABASE_NAME = "contohdatabase.db";
    private static final Integer DATABASE_VERSION = 1;

    public enum Tabel
    {
        pertanyaan("pertanyaan"),
        penyakit("penyakit");

        private final String text;

        private Tabel(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    public DatabaseHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        sqLiteDatabase.execSQL("CREATE TABLE " + Tabel.pertanyaan +
                " (kode INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "pertanyaan VARCHAR(500));");

        sqLiteDatabase.execSQL("CREATE TABLE " + Tabel.penyakit +
                " (kode INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "nama VARCHAR(500), solusi TEXT, gambar INTEGER);");

        sqLiteDatabase.execSQL("INSERT INTO " + Tabel.pertanyaan + " (pertanyaan) VALUES " +
                "('Apakah Anda merasa gatal ?')," +
                "('Apakah Anda merasa pegal² di sekitar anus ?')," +
                "('Apakah Anda mengalami kejang² saat melihat mantan Anda menikah ?')," +
                "('Apakah Anda merasakan apa yang aku rasakan ?');");

        sqLiteDatabase.execSQL("INSERT INTO " + Tabel.penyakit + " (nama, solusi, gambar) VALUES " +
                "('Gila', 'Anda perlu mempunyai seorang kekasih', " + R.drawable.jantung + ")," +
                "('Rematik', 'Solusi Solusi Solusi Solusi Solusi Solusi Solusi Solusi Solusi Solusi ', " + R.drawable.rematik + ")," +
                "('Kudis', 'Solusi Solusi Solusi Solusi Solusi Solusi Solusi Solusi Solusi Solusi ', " + R.drawable.kudis + ")," +
                "('Stroke', 'Solusi Solusi Solusi Solusi Solusi Solusi Solusi Solusi Solusi Solusi ', " + R.drawable.stroke + ")," +
                "('Sehat', 'Solusi Solusi Solusi Solusi Solusi Solusi Solusi Solusi Solusi Solusi ', " + R.drawable.sehat + ");");


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1)
    {

    }
}

