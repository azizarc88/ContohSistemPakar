package id.backbonedev.contohsistempakar.Data;

/**
 * Created by Aziz Nur Ariffianto on 0025, 25 Jul 2017.
 */

public class DataPenyakit
{
    String nama, solusi;
    Integer gambar;

    public DataPenyakit(String nama, String solusi, Integer gambar)
    {
        this.nama = nama;
        this.solusi = solusi;
        this.gambar = gambar;
    }

    public DataPenyakit()
    {
    }

    public String getNama()
    {
        return nama;
    }

    public void setNama(String nama)
    {
        this.nama = nama;
    }

    public String getSolusi()
    {
        return solusi;
    }

    public void setSolusi(String solusi)
    {
        this.solusi = solusi;
    }

    public Integer getGambar()
    {
        return gambar;
    }

    public void setGambar(Integer gambar)
    {
        this.gambar = gambar;
    }
}
