package id.backbonedev.contohsistempakar;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.backbonedev.contohsistempakar.Data.DataPenyakit;
import id.backbonedev.contohsistempakar.Database.DatabaseHelper;

public class MainActivity extends AppCompatActivity
{
    private Button BYa, BTidak;
    private TextView LSoal;
    private List<String> kumpulansoal;
    private List<DataPenyakit> kumpulanpenyakit;
    private List<Boolean> kumpulanjawaban;
    private int posisisoal = 0;

    private Cursor cursor;
    private DatabaseHelper db;
    private SQLiteDatabase database;

    private List<String> Kalkulasi()
    {
        List<String> hasil = new ArrayList<>();

        if (kumpulanjawaban.get(0) == true && kumpulanjawaban.get(1) == true && kumpulanjawaban.get(2) == true && kumpulanjawaban.get(3) == true)
        {
            hasil.add(kumpulanpenyakit.get(0).getNama());
            hasil.add(kumpulanpenyakit.get(0).getSolusi());
            hasil.add(kumpulanpenyakit.get(0).getGambar().toString());
        }
        else if (kumpulanjawaban.get(0) == false && kumpulanjawaban.get(1) == true && kumpulanjawaban.get(2) == true && kumpulanjawaban.get(3) == false)
        {
            hasil.add(kumpulanpenyakit.get(1).getNama());
            hasil.add(kumpulanpenyakit.get(1).getSolusi());
            hasil.add(kumpulanpenyakit.get(1).getGambar().toString());
        }
        else if (kumpulanjawaban.get(0) == true && kumpulanjawaban.get(1) == false && kumpulanjawaban.get(2) == true && kumpulanjawaban.get(3) == false)
        {
            hasil.add(kumpulanpenyakit.get(2).getNama());
            hasil.add(kumpulanpenyakit.get(2).getSolusi());
            hasil.add(kumpulanpenyakit.get(2).getGambar().toString());
        }
        else if (kumpulanjawaban.get(0) == true && kumpulanjawaban.get(1) == false && kumpulanjawaban.get(2) == false && kumpulanjawaban.get(3) == true)
        {
            hasil.add(kumpulanpenyakit.get(3).getNama());
            hasil.add(kumpulanpenyakit.get(3).getSolusi());
            hasil.add(kumpulanpenyakit.get(3).getGambar().toString());
        }
        else
        {
            hasil.add(kumpulanpenyakit.get(4).getNama());
            hasil.add(kumpulanpenyakit.get(4).getSolusi());
            hasil.add(kumpulanpenyakit.get(4).getGambar().toString());
        }

        return hasil;
    }

    private void SoalSelanjutnya()
    {
        if (posisisoal < kumpulansoal.size() - 1)
        {
            posisisoal++;
            LSoal.setText(kumpulansoal.get(posisisoal));
        }
        else
        {
            Toast.makeText(this, "Soal sudah mentok", Toast.LENGTH_SHORT).show();
            List<String> hasil = Kalkulasi();

            Intent intent = new Intent(getApplicationContext(), HasilAnalisaActivity.class);
            intent.putExtra("keterangan", hasil.get(1));
            intent.putExtra("penyakit", hasil.get(0));
            intent.putExtra("gambar", Integer.valueOf(hasil.get(2)));
            startActivityForResult(intent, 100);
        }
    }

    private void AmbilData()
    {
        cursor = database.rawQuery("SELECT * FROM " + DatabaseHelper.Tabel.pertanyaan, null);

        if (cursor.moveToFirst())
        {
            while (!cursor.isAfterLast())
            {
                String pertanyaan = cursor.getString(cursor.getColumnIndex("pertanyaan"));
                kumpulansoal.add(pertanyaan);
                cursor.moveToNext();
            }
        }

        cursor = database.rawQuery("SELECT * FROM " + DatabaseHelper.Tabel.penyakit, null);

        if (cursor.moveToFirst())
        {
            while (!cursor.isAfterLast())
            {
                String nama = cursor.getString(cursor.getColumnIndex("nama"));
                String solusi = cursor.getString(cursor.getColumnIndex("solusi"));
                String gambar = cursor.getString(cursor.getColumnIndex("gambar"));
                kumpulanpenyakit.add(new DataPenyakit(nama, solusi, Integer.valueOf(gambar)));
                cursor.moveToNext();
            }
        }

        LSoal.setText(kumpulansoal.get(posisisoal));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        kumpulanpenyakit = new ArrayList<>();
        kumpulansoal = new ArrayList<>();
        kumpulanjawaban = new ArrayList<>();
        db = new DatabaseHelper(this);
        database = db.getReadableDatabase();

        BYa = (Button) findViewById(R.id.BYa);
        BTidak = (Button) findViewById(R.id.BTidak);
        LSoal = (TextView) findViewById(R.id.LSoal);

        BYa.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                kumpulanjawaban.add(true);
                SoalSelanjutnya();
            }
        });

        BTidak.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View view)
            {
                kumpulanjawaban.add(false);

                SoalSelanjutnya();
            }
        });

        AmbilData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == RESULT_OK)
        {
            if (requestCode == 100)
            {
                posisisoal = -1;
                kumpulanjawaban.clear();
                SoalSelanjutnya();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
